#include "helper.h"
#include <iostream>
#include <windows.h>
#include "Shlwapi.h"

void checkArgs(int argc, char *argv[], std::string *fName) {
    if (argc > 1) {
        *fName = argv[1];
    }
}

bool getDriveLetter(std::string* fName) {
    DWORD dwSize = MAX_PATH;
    char szData[MAX_PATH] = { 0 };
    DWORD dwResult = GetLogicalDriveStrings(dwSize, szData);
    std::list<std::string> drives;
    std::list<std::string>::iterator it;
    int i, selection;

    if (dwResult > 0 && dwResult <= MAX_PATH) {
        char *szTmpData = szData;
        while (*szTmpData) {
            drives.push_back(szTmpData);
            szTmpData += strlen(szTmpData) + 1;
        }
    } else {
        return false;
    }
    while (true) {
        for (it = drives.begin(), i = 0; it != drives.end(); it++, i++)
            std::cout << "[" << i << "]\t" << *it << std::endl;
        std::cout << "[" << i << "]\tCANCEL\n\n";
        std::cin >> selection;
        if (selection == i)
            return false;
        else if (selection > 0 || selection < i) {
            it = drives.begin();
            advance(it, selection);
            *fName = *it;
            return true;
        }
    }
}

bool getFilesInDirectory(const std::string &fName, std::list<std::string> *files) {
    std::string findString = fName + '*';
    HANDLE hFind = INVALID_HANDLE_VALUE;
    WIN32_FIND_DATA ffd;
    DWORD dwError = 0;

    hFind = FindFirstFile(findString.c_str(), &ffd);
    if (hFind == INVALID_HANDLE_VALUE)
        return false;

    do {
        files->push_back(ffd.cFileName);
    } while (FindNextFile(hFind, &ffd) != 0);

    dwError = GetLastError();
    if (dwError != ERROR_NO_MORE_FILES) {
        FindClose(hFind);
        return false;
    }

    FindClose(hFind);
    return true;
}

bool getFileNameManually(std::string* fName) {
    if (!getDriveLetter(fName))
        return false;

    std::list<std::string> files;
    std::list<std::string>::iterator it;
    int i, selection;
    while (true) {
        if (!getFilesInDirectory(*fName, &files))
            return false;
        while (true) {
            for (it = files.begin(), i = 0; it != files.end(); it++, i++)
                std::cout << "[" << i << "]\t" << *it << std::endl;
            std::cout << "[" << i << "]\tCANCEL\n\n";
            std::cin >> selection;
            if (selection == i)
                return false;
            else if (selection > 0 || selection < i) {
                it = files.begin();
                advance(it, selection);
                *fName += *it;
                files.clear();
                if (!PathFileExists(fName->c_str()))
                    return false;
                else if (!PathIsDirectory(fName->c_str()))
                    return true;
                else
                    *fName += "\\";
                break;
            }
        }
    }
    return true;
}
