#include "directorydesc.h"

DirectoryDesc::DirectoryDesc(std::string path, DirectoryDesc *parent /*= nullptr*/) :
_path{ path },
_parent{ parent } { }

DirectoryDesc::~DirectoryDesc() { }

std::string DirectoryDesc::path() const {
    return _parent ? _parent->path() + "\\" + _path : _path;
}

std::string DirectoryDesc::name() const {
    return _parent ? _path : std::string(find(_path.rbegin(), _path.rend(), '\\').base(), _path.end());
}
