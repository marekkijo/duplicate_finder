#include <iostream>
#include <fstream>
#include <algorithm>
#include <windows.h>
#include "Shlwapi.h"
#include "duplicatefinder.h"
#include "directorydesc.h"
#include "filedesc.h"

const std::vector<unsigned int> DuplicateFinder::_steps{
    1000,
    0 };

DuplicateFinder::DuplicateFinder() :
_root{ nullptr } { }

DuplicateFinder::~DuplicateFinder() {
    clean();
}

void DuplicateFinder::findDuplicates() {
    clean();
    _root = new DirectoryDesc(_path);
    rebuildDirectory(_root);

    std::list<FileDesc *>::iterator beginIt = _files.begin();
    std::list<FileDesc *> sameSizeFiles;
    while(true) {
        sameSizeFiles = findDuplicatesBySize(&beginIt);
        if (sameSizeFiles.empty())
            break;
        findDuplicatesByComparison(sameSizeFiles);
    }

    std::ofstream ofs1{ "chuj2.txt", std::ofstream::out | std::ofstream::trunc };
    for (const auto file : _duplicatedFiles)
        ofs1 << file->path() << std::endl;
    //ofs1 << file->path() << "\tHASH:" << file->hashToString() << "\t(" << file->size() << "B)" << std::endl;
}

void DuplicateFinder::rebuildDirectory(DirectoryDesc *dir) {
    auto elems = getPathElements(dir->path());
    for (const auto &elem : elems) {
        if (elem.isDir) {
            dir->dirs().push_back(DirectoryDesc(elem.name, dir));
            _dirs.push_back(&dir->dirs().back());
            rebuildDirectory(&dir->dirs().back());
        } else {
            dir->files().push_back(FileDesc(elem.name, dir));
            dir->files().back().size(elem.size);
            _files.push_back(&dir->files().back());
        }
    }
}

void DuplicateFinder::clean() {
    delete _root;
    _dirs.clear();
    _files.clear();
    _duplicatedFiles.clear();
}

std::list<DuplicateFinder::PathElementInfo> DuplicateFinder::getPathElements(std::string path) {
    WIN32_FIND_DATA ffd;
    std::list<DuplicateFinder::PathElementInfo> elems;

    HANDLE hfind = FindFirstFile((path + "\\*").c_str(), &ffd);
    if (hfind == INVALID_HANDLE_VALUE)
        return elems;
    
    do {
        elems.push_back({ ffd.cFileName, ffd.nFileSizeLow, static_cast<bool>(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) });
    } while (FindNextFile(hfind, &ffd) != 0);

    FindClose(hfind);
    {
        auto it = find_if(elems.begin(), elems.end(), [](const DuplicateFinder::PathElementInfo &val) { return val.name == "."; });
        if (it != elems.end()) elems.erase(it);
    }
    {
        auto it = find_if(elems.begin(), elems.end(), [](const DuplicateFinder::PathElementInfo &val) { return val.name == ".."; });
        if (it != elems.end()) elems.erase(it);
    }
    return elems;
}

std::list<FileDesc *> DuplicateFinder::findDuplicatesBySize(std::list<FileDesc *>::iterator *beginIt) {
    std::list<FileDesc *> retFiles;
    unsigned int size;
    std::list<FileDesc *>::iterator it_in;
    for (std::list<FileDesc *>::iterator it = *beginIt; it != _files.end(); it++) {
        size = (*it)->size();
        it_in = std::next(it);
        while (true) {
            it_in = find_if(it_in, _files.end(), [&size](const FileDesc *pfile) { return pfile->size() == size; });
            if (it_in != _files.end()) {
                retFiles.push_back(*it_in);
                it_in = _files.erase(it_in);
            } else {
                break;
            }
        }
        if (!retFiles.empty()) {
            retFiles.push_front(*it);
            *beginIt = _files.erase(it);
            return retFiles;
        }
    }
    return retFiles;
}

void DuplicateFinder::findDuplicatesByComparison(std::list<FileDesc *> &files) {
    std::list<FileDesc *> swapFiles;
    std::list<FileDesc *>::iterator it_in;
    bool found = false;

    for (unsigned int i = 0; i < _steps.size(); i++) {
        for (auto file : files)
            file->genFileHash(i == 0 ? 0 : _steps[i - 1], _steps[i]);
        for (std::list<FileDesc *>::iterator it = files.begin(); it != files.end();) {
            found = false;
            it_in = std::next(it);
            while (true) {
                it_in = find_if(it_in, files.end(), [&it](const FileDesc *pfile) {
                    return
                        pfile->isHashDefinitive() == (*it)->isHashDefinitive() &&
                        pfile->compareHash(**it);
                });

                if (it_in != files.end()) {
                    found = true;
                    if ((*it_in)->isHashDefinitive())
                        _duplicatedFiles.push_back(*it_in);
                    else
                        swapFiles.push_back(*it_in);
                    it_in = files.erase(it_in);
                } else {
                    break;
                }
            }
            if (found) {
                if ((*it)->isHashDefinitive())
                    _duplicatedFiles.push_back(*it);
                else
                    swapFiles.push_back(*it);
                it = files.erase(it);
            } else {
                it++;
            }
        }
        files.swap(swapFiles);
        swapFiles.clear();
        if (files.empty())
            break;
    }
}
