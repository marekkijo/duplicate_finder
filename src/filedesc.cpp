#include <cassert>
#include <iostream>
#include <typeinfo>
#include <cstring>
#include <sstream>
#include <fstream>
#include <iomanip>
#include "filedesc.h"
#include "directorydesc.h"

FileDesc::FileDesc(std::string path, DirectoryDesc *parent) :
_path{ path },
_parent{ parent },
_size{ 0 } {
    assert(_parent);
}

FileDesc::~FileDesc() { }

void FileDesc::genFileHash(unsigned int from, unsigned int to) {
    std::ifstream ifs{ path(), std::ifstream::in | std::ifstream::binary };
    ifs >> std::noskipws;

    if (from == 0) {
        memset(_hash, 0U, FileDesc::HASH_SIZE);
        _definitiveHash = false;
    } else {
        ifs.seekg(from);
    }

    std::istream_iterator<unsigned char> ii{ ifs };

    if (to == 0 || to >= _size) {
        std::istream_iterator<unsigned char> eos{ };
        for (int i = from % FileDesc::HASH_SIZE; ii != eos; ++ii, ++i)
            _hash[i % FileDesc::HASH_SIZE] ^= *ii;
        _definitiveHash = true;
    } else {
        for (unsigned int i = from % FileDesc::HASH_SIZE; i < to; ++ii, ++i)
            _hash[i % FileDesc::HASH_SIZE] ^= *ii;
    }
}

std::string FileDesc::hashToString() const {
    std::stringstream ss;
    ss << std::setiosflags(std::ios::uppercase) << std::setw(2) << std::setfill('0') << std::hex;
    for (int i = 0; i != FileDesc::HASH_SIZE; ++i)
        ss << static_cast<unsigned int>(_hash[i]);
    return ss.str();
}

bool FileDesc::compareHash(const FileDesc &other) const {
    return memcmp(_hash, other._hash, FileDesc::HASH_SIZE) == 0;
}

std::string FileDesc::path() const {
    return _parent->path() + "\\" + _path;
}

std::string FileDesc::ext() const {
    auto r_it = find(_path.rbegin(), _path.rend(), '.');
    if (r_it == _path.rend()) return std::string();
    return std::string(r_it.base(), _path.end());
}
