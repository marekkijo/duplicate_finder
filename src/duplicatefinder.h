#ifndef duplicatefinder_h__
#define duplicatefinder_h__

#include <string>
#include <list>
#include <vector>

class FileDesc;
class DirectoryDesc;

class DuplicateFinder {
public:
    DuplicateFinder();
    ~DuplicateFinder();
    std::string path() const { return _path; }
    void path(std::string val) { _path = val; }
    void findDuplicates();

private:
    struct PathElementInfo {
        std::string name;
        unsigned int size;
        bool isDir;
    };
    void clean();
    void rebuildDirectory(DirectoryDesc *dir);
    static const std::vector<unsigned int> _steps;
    std::list<PathElementInfo> getPathElements(std::string path);
    std::list<FileDesc *> findDuplicatesBySize(std::list<FileDesc *>::iterator *beginIt);
    void findDuplicatesByComparison(std::list<FileDesc *> &files);
    std::string _path;
    DirectoryDesc *_root;
    std::list<DirectoryDesc *> _dirs;
    std::list<FileDesc *> _files;
    std::list<FileDesc *> _duplicatedFiles;

};

#endif // duplicatefinder_h__
