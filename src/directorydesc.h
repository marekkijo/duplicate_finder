#ifndef directorydesc_h__
#define directorydesc_h__

#include <string>
#include <list>
#include "filedesc.h"

class DirectoryDesc {
public:
    DirectoryDesc(std::string path, DirectoryDesc *parent = nullptr);
    ~DirectoryDesc();
    std::string path() const;
    std::string name() const;
    std::list<DirectoryDesc> &dirs() { return _dirs; }
    std::list<FileDesc> &files() { return _files; }

private:
    std::string _path;
    DirectoryDesc *_parent;
    std::list<DirectoryDesc> _dirs;
    std::list<FileDesc> _files;
};

#endif // directorydesc_h__
