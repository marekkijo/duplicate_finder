#ifndef filedesc_h__
#define filedesc_h__

#include <string>

class DirectoryDesc;

class FileDesc {
public:
    enum { HASH_SIZE = 20 };
    FileDesc(std::string path, DirectoryDesc *parent);
    ~FileDesc();
    void genFileHash(unsigned int from, unsigned int to);
    bool isHashDefinitive() const { return _definitiveHash; };
    std::string hashToString() const;
    bool compareHash(const FileDesc &other) const;
    std::string path() const;
    std::string name() const { return _path; };
    std::string ext() const;
    unsigned int size() const { return _size; }
    void size(unsigned int val) { _size = val; }

private:
    std::string _path;
    DirectoryDesc *_parent;
    unsigned int _size;
    unsigned char _hash[HASH_SIZE];
    bool _definitiveHash;
};

#endif // filedesc_h__
