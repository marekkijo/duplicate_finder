#include <vector>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <iostream>
#include "Shlwapi.h"
#include "duplicatefinder.h"

const unsigned int HASH_SIZE{ 20 };

std::vector<unsigned char> getFileHash(const std::string &fName) {
    std::vector<unsigned char> hash(HASH_SIZE, 0U);
    std::ifstream ifs{ fName, std::ifstream::in | std::ifstream::binary };
    std::istream_iterator<unsigned char> ii{ ifs };
    std::istream_iterator<unsigned char> eos{ };
    for (int i = 0; ii != eos && i < 1000; ++ ii, ++ i)
        hash[i % HASH_SIZE] ^= *ii;
    return hash;
}

std::string hashToString(const std::vector<unsigned char> &fHash) {
    if (fHash.size() < HASH_SIZE)
        return std::string();
    std::stringstream ss;
    for (int i = 0; i != HASH_SIZE; ++i)
        ss << std::setw(2) << std::setfill('0') << std::hex
           << static_cast<unsigned int>(fHash[i]);
    return ss.str();
}

int main(int argc, char *argv[]) {
    DuplicateFinder df;
    if (argc > 1) {
        df.path(argv[1]);
        df.findDuplicates();
    }
    std::cout << "Press any key to continue . . .\n";
    std::cin.sync();
    std::cin.get();
    return 0;
    /*

    std::vector<std::string> fNames;
    std::vector<std::vector<unsigned char>> fHashes;
    std::vector<std::string> fHashStrings;
    if (argc < 3) {
        std::cout << "Please give two filenames as an arguments.\n";
        goto some_error;
    }
    for (int i = 1; i < argc; ++i)
        fNames.push_back(argv[i]);
    for (const auto &fName : fNames) {
        if (!PathFileExists(fName.c_str())) {
            std::cout << fName << " is not a proper file path.\n";
            goto some_error;
        }
        if (PathIsDirectory(fName.c_str())) {
            std::cout << fName << " is a directory not a file.\n";
            goto some_error;
        }
    }
    for (const auto &fName : fNames) {
        fHashes.push_back(getFileHash(fName));
        fHashStrings.push_back(hashToString(fHashes.back()));
        std::cout << fHashStrings.back() << std::endl;// << " : " << fName << std::endl;
    }
    std::cout << std::endl;
    for (unsigned int i = 0; i < fNames.size(); ++i) {
        for (unsigned int j = i + 1; j < fNames.size(); ++j) {
            if (equal(fHashes[i].begin(), fHashes[i].end(), fHashes[j].begin())) {
                std::cout << "file " << fNames[i] << std::endl << "is the same as file " << fNames[j] << std::endl << std::endl;
            }
        }
    }

//#ifdef _DEBUG
    std::cout << "Press any key to continue . . .\n";
    std::cin.sync();
    std::cin.get();
//#endif

    return 0;

some_error:
    std::cout << "Press any key to continue . . .\n";
    std::cin.sync();
    std::cin.get();

    return 1;*/
}