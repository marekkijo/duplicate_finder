#ifndef helper_h__
#define helper_h__

#include <string>
#include <list>

void checkArgs(int argc, char *argv[], std::string *fName);
bool getDriveLetter(std::string* fName);

bool getFilesInDirectory(const std::string &fName, std::list<std::string> *files);

bool getFileNameManually(std::string* fName);

#endif // helper_h__
